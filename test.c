#include <stdio.h>
#include <string.h>
#include <czmq.h>
#include "prometheuz.h"

void *sender(void *arg) {
    zsock_t *sender = zsock_new_push ("inproc://demo");
    while (1) {
        zstr_send (sender, "hello");
    }
}

int main(int argc, char *argv[]) {
    GoString namespace = {"zmq", 3};
    StartPrometheuz(namespace);

    zsock_t *receiver = zsock_new_pull ("inproc://demo");
    
    pthread_t tid[1];
    pthread_create (&(tid[0]), NULL, &sender, NULL);
    
    printf("starting\n");
    int i = 0;
    while (1) {
        char *msg = zstr_recv (receiver);
        free (msg);
        i++;
        if (!(i % 1000)) {
            AddMsgs(1000);
            AddBytes(50000);
        }
    }
}

