package main

import "C"
import (
	"net/http"

	"github.com/prometheus/client_golang/prometheus"
)

var ctx *prometheuz

type prometheuz struct {
	stats *promStats
}

func (z *prometheuz) addMsgs(msgs float64) {
	z.stats.msgs.Add(msgs)
}

func (z *prometheuz) addBytes(bytes float64) {
	z.stats.bytes.Add(bytes)
}

type promStats struct {
	msgs  prometheus.Counter
	bytes prometheus.Counter
}

func (p *promStats) register() {
	prometheus.MustRegister(p.msgs)
	prometheus.MustRegister(p.bytes)
}

func newStats(namespace string) *promStats {
	stats := &promStats{
		msgs: prometheus.NewCounter(prometheus.CounterOpts{
			Namespace: namespace,
			Name:      "msgs_total",
			Help:      "Message Count",
		}),
		bytes: prometheus.NewCounter(prometheus.CounterOpts{
			Namespace: namespace,
			Name:      "bytes_total",
			Help:      "Bytes Count",
		}),
	}

	stats.register()
	return stats
}

//export StartPrometheuz
func StartPrometheuz(namespace string) {
	ctx = &prometheuz{
		stats: newStats(namespace),
	}

	go func() {
		http.Handle("/metrics", prometheus.Handler())
		http.ListenAndServe(":9999", nil)
	}()
}

//export AddMsgs
func AddMsgs(count float64) {
	ctx.addMsgs(count)
}

//export AddBytes
func AddBytes(count float64) {
	ctx.addBytes(count)
}

func main() {}
